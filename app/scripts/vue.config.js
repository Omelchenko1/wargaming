Vue.config.KS_CONFIG = {
	serviceCenters: [
  {
    "town": "Александрия",
    "servicesCenters": [
      {
        "address": "пр. Соборний (Леніна), 85"
      }
    ]
  },
  {
    "town": "Андрушевка",
    "servicesCenters": [
      {
        "address": "вул. Зазулинського, 9а"
      }
    ]
  },
  {
    "town": "Апостолово",
    "servicesCenters": [
      {
        "address": "вул. Ветеранів, 29а"
      }
    ]
  },
  {
    "town": "Балаклея",
    "servicesCenters": [
      {
        "address": "вул. Жовтнева, 37"
      }
    ]
  },
  {
    "town": "Бар",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 6б"
      }
    ]
  },
  {
    "town": "Бахмач",
    "servicesCenters": [
      {
        "address": " вул. Першотравнева, 41а"
      }
    ]
  },
  {
    "town": "Бахмут",
    "servicesCenters": [
      {
        "address": "вул. Торгова (був. Воровського), 1"
      }
    ]
  },
  {
    "town": "Белая Церковь",
    "servicesCenters": [
      {
        "address": "вул. Ярослава Мудрого, 21а"
      },
      {
        "address": "бул. Олександрійський (пр.50-річчя Перемоги), 95-97"
      },
      {
        "address": "вул. Леваневського, 59"
      }
    ]
  },
  {
    "town": "Белгород-Днестровский",
    "servicesCenters": [
      {
        "address": "вул. Першотравнева, 86/88"
      }
    ]
  },
  {
    "town": "Бердичев",
    "servicesCenters": [
      {
        "address": "вул. Європейська, 10"
      }
    ]
  },
  {
    "town": "Бердянск",
    "servicesCenters": [
      {
        "address": "вул. Італійська (був. Дюміна), 48/28"
      }
    ]
  },
  {
    "town": "Бережаны",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 3/2"
      }
    ]
  },
  {
    "town": "Березне",
    "servicesCenters": [
      {
        "address": "ул. Андреевская, 5"
      }
    ]
  },
  {
    "town": "Бершадь",
    "servicesCenters": [
      {
        "address": "вул. Ярослава Мудрого (був. Леніна), 1а"
      }
    ]
  },
  {
    "town": "Благовещенское (Ульяновка)",
    "servicesCenters": [
      {
        "address": "вул. Героїв України (Леніна), 58"
      }
    ]
  },
  {
    "town": "Богодухов",
    "servicesCenters": [
      {
        "address": "вул. Кірова, 44"
      }
    ]
  },
  {
    "town": "Богуслав",
    "servicesCenters": [
      {
        "address": "вул. І. Франка, 22/1"
      }
    ]
  },
  {
    "town": "Болград",
    "servicesCenters": [
      {
        "address": "пр. Соборний (Леніна), 109"
      }
    ]
  },
  {
    "town": "Борзна",
    "servicesCenters": [
      {
        "address": " вул. Леніна, 1"
      }
    ]
  },
  {
    "town": "Борисполь",
    "servicesCenters": [
      {
        "address": "вул. Київський Шлях, 2/6 ТЦ Аеромолл"
      },
      {
        "address": "Київський шлях, 67 ТРЦ Парк Таун"
      }
    ]
  },
  {
    "town": "Борщев",
    "servicesCenters": [
      {
        "address": "вул. Верхратського,3д"
      }
    ]
  },
  {
    "town": "Боярка",
    "servicesCenters": [
      {
        "address": "вул. Хрещатик, 43"
      }
    ]
  },
  {
    "town": "Бровары",
    "servicesCenters": [
      {
        "address": "бул. Незалежності, 2"
      }
    ]
  },
  {
    "town": "Броды",
    "servicesCenters": [
      {
        "address": "вул. Золота, 1"
      }
    ]
  },
  {
    "town": "Бурштын",
    "servicesCenters": [
      {
        "address": " вул. Калуська, 5а супермаркет Гулівер"
      }
    ]
  },
  {
    "town": "Бучач",
    "servicesCenters": [
      {
        "address": " вул. Підгаєцька, 6"
      }
    ]
  },
  {
    "town": "Вараш (Кузнецовск)",
    "servicesCenters": [
      {
        "address": "мкрн. Вараш, 11/2"
      }
    ]
  },
  {
    "town": "Васильков",
    "servicesCenters": [
      {
        "address": "вул. Грушевського, 8/1"
      }
    ]
  },
  {
    "town": "Васильковка",
    "servicesCenters": [
      {
        "address": "вул. Першотравнева, 201б"
      }
    ]
  },
  {
    "town": "Ватутино",
    "servicesCenters": [
      {
        "address": "вул. Франка, 1/11"
      }
    ]
  },
  {
    "town": "Великая Новоселка",
    "servicesCenters": [
      {
        "address": "вул. Центральна (був. Жовтнева), 36"
      }
    ]
  },
  {
    "town": "Великий Бычков",
    "servicesCenters": [
      {
        "address": "вул. Грушевського, 113 а, ТЦ Шафран"
      }
    ]
  },
  {
    "town": "Верховина",
    "servicesCenters": [
      {
        "address": "вул. І. Франка, 1"
      }
    ]
  },
  {
    "town": "Вижница",
    "servicesCenters": [
      {
        "address": "вул. Пушкіна, 1"
      }
    ]
  },
  {
    "town": "Винница",
    "servicesCenters": [
      {
        "address": "вул. Келецька, 78в (пр. Юності) ТРЦ Магігранд"
      },
      {
        "address": "вул. Замостянська (50-річчя Перемоги), 27"
      },
      {
        "address": "вул. Пирогова, 105"
      },
      {
        "address": "пр. Коцюбинського, 19"
      },
      {
        "address": "вул. Миколи Оводова (Козицького), 51 ТРЦ Sky Park"
      },
      {
        "address": "ул. Соборная, 46"
      }
    ]
  },
  {
    "town": "Виноградов",
    "servicesCenters": [
      {
        "address": " вул. Шевченка, 3"
      }
    ]
  },
  {
    "town": "Вишневое",
    "servicesCenters": [
      {
        "address": "вул. Європейська (Жовтнева), 31а"
      }
    ]
  },
  {
    "town": "Владимир-Волынский",
    "servicesCenters": [
      {
        "address": "вул. Ковельська, 12"
      }
    ]
  },
  {
    "town": "Вознесенск",
    "servicesCenters": [
      {
        "address": "вул. Київська (Жовтневої революції), 3а"
      }
    ]
  },
  {
    "town": "Вольногорск",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 65к"
      }
    ]
  },
  {
    "town": "Волочиск",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 25"
      }
    ]
  },
  {
    "town": "Вышгород",
    "servicesCenters": [
      {
        "address": "вул. Мазепи, 1"
      }
    ]
  },
  {
    "town": "Гайворон",
    "servicesCenters": [
      {
        "address": "вул. Енергетична, 21/26б"
      }
    ]
  },
  {
    "town": "Геническ",
    "servicesCenters": [
      {
        "address": "вул. Дружби народів (був. Махарадзе), 64а"
      }
    ]
  },
  {
    "town": "Гнивань",
    "servicesCenters": [
      {
        "address": " вул. Соборна (Леніна), 58"
      }
    ]
  },
  {
    "town": "Голованевск",
    "servicesCenters": [
      {
        "address": " вул. Леніна, (навпроти ТЦ Дитячий Світ)"
      }
    ]
  },
  {
    "town": "Горишние Плавни (Комсомольск)",
    "servicesCenters": [
      {
        "address": "вул. Героїв Дніпра (був. Леніна), 77"
      }
    ]
  },
  {
    "town": "Городня",
    "servicesCenters": [
      {
        "address": "вул. Першого травня, 14а"
      }
    ]
  },
  {
    "town": "Городок",
    "servicesCenters": [
      {
        "address": "вул. Перемишльська, 5в"
      }
    ]
  },
  {
    "town": "Горохов",
    "servicesCenters": [
      {
        "address": "ул.Шевченко, 21"
      }
    ]
  },
  {
    "town": "Днепр",
    "servicesCenters": [
      {
        "address": "вул. Тітова, 11"
      },
      {
        "address": "пр. Героїв, 1к"
      },
      {
        "address": "вул. Глінки, 2 ТРЦ Мост Сіті"
      },
      {
        "address": "пр. Слобожанський (кол. ім. Газети \"Правда\"), 87"
      },
      {
        "address": "пр. Дмитра Яворницького (кол. К.Маркса), 65 "
      },
      {
        "address": "бул. Зоряний, 1а ТРЦ Дафі"
      },
      {
        "address": "пр. Дмитра Яворницького (був. К.Маркса), 113"
      },
      {
        "address": "пр. Богдана Хмельницкого (Героїв Сталінграда), 118д ТРЦ Терра"
      },
      {
        "address": "вул. Нижньодніпровська, 17 ТРЦ Караван"
      },
      {
        "address": " вул. Тітова, 36 ТРЦ Appolo"
      },
      {
        "address": "вул.Д.Яворницкого,52 ЦУМ"
      },
      {
        "address": "вул. Пастера, 6а"
      }
    ]
  },
  {
    "town": "Долина",
    "servicesCenters": [
      {
        "address": "пр. Незалежності, 8а"
      }
    ]
  },
  {
    "town": "Долинская",
    "servicesCenters": [
      {
        "address": "вул. Центральна (Радянська), 144/10"
      }
    ]
  },
  {
    "town": "Драбов",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 32"
      }
    ]
  },
  {
    "town": "Дрогобыч",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 6"
      }
    ]
  },
  {
    "town": "Дубно",
    "servicesCenters": [
      {
        "address": "вул. Д. Галицького, 4"
      }
    ]
  },
  {
    "town": "Дубовое",
    "servicesCenters": [
      {
        "address": "вул. Гагаріна, 13а"
      }
    ]
  },
  {
    "town": "Дунаевцы",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 57"
      }
    ]
  },
  {
    "town": "Энергодар",
    "servicesCenters": [
      {
        "address": "вул. Молодіжна (Комсомольська), 43/1"
      }
    ]
  },
  {
    "town": "Желтые Воды",
    "servicesCenters": [
      {
        "address": "вул. Кропоткіна, 39/37"
      }
    ]
  },
  {
    "town": "Житомир",
    "servicesCenters": [
      {
        "address": "вул. Київська, 64"
      },
      {
        "address": "вул. Київська, 1"
      },
      {
        "address": "пл.Житній ринок,1, ТЦ Житній"
      },
      {
        "address": "вул. Київська, 39 "
      },
      {
        "address": "вул. Київська, 77, ТЦ Глобал UA"
      }
    ]
  },
  {
    "town": "Жмеринка",
    "servicesCenters": [
      {
        "address": "вул. Пушкіна, 9"
      }
    ]
  },
  {
    "town": "Запорожье",
    "servicesCenters": [
      {
        "address": "вул. Задніпровська, 9-а"
      },
      {
        "address": "вул. Ладозька, 4"
      },
      {
        "address": "пр. Соборний, 147 ТРЦ Україна"
      },
      {
        "address": "пр. Соборний (був. Леніна), 179"
      },
      {
        "address": "бул. Центральний, 3"
      },
      {
        "address": "вул. Запорізька, 1 Б ТРЦ Сіті Молл"
      },
      {
        "address": "пр. Соборний, 87з"
      },
      {
        "address": "пр.Соборний (Леніна),42 "
      }
    ]
  },
  {
    "town": "Збараж",
    "servicesCenters": [
      {
        "address": "вул І. Франка, 18"
      }
    ]
  },
  {
    "town": "Звенигородка",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 66б"
      }
    ]
  },
  {
    "town": "Здолбунов",
    "servicesCenters": [
      {
        "address": "вул. Б. Хмельницького, 27б"
      }
    ]
  },
  {
    "town": "Знаменка",
    "servicesCenters": [
      {
        "address": "вул. Калініна, 118"
      }
    ]
  },
  {
    "town": "Золотоноша",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 96 "
      }
    ]
  },
  {
    "town": "Золочев",
    "servicesCenters": [
      {
        "address": "пл. Вічева, 1"
      }
    ]
  },
  {
    "town": "Иванков",
    "servicesCenters": [
      {
        "address": "вул. Київська, 26"
      }
    ]
  },
  {
    "town": "Ивано-Франковск",
    "servicesCenters": [
      {
        "address": "вул. Галицька, 9"
      },
      {
        "address": "вул. Січових Стрільців, 30"
      },
      {
        "address": "вул. Миколайчука, 2, ТЦ Арсен"
      },
      {
        "address": "вул. Шашкевича, 7 "
      }
    ]
  },
  {
    "town": "Измаил",
    "servicesCenters": [
      {
        "address": "ул. Болградская, 80"
      },
      {
        "address": " пр. Суворова, 37"
      }
    ]
  },
  {
    "town": "Изюм",
    "servicesCenters": [
      {
        "address": "вул. Гагаріна, 9б"
      }
    ]
  },
  {
    "town": "Изяслав",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 19"
      }
    ]
  },
  {
    "town": "Ильинцы",
    "servicesCenters": [
      {
        "address": "вул.Європейська (К. Маркса), 15"
      }
    ]
  },
  {
    "town": "Ирпень",
    "servicesCenters": [
      {
        "address": " вул. Центральна, 1"
      }
    ]
  },
  {
    "town": "Иршава",
    "servicesCenters": [
      {
        "address": "вул. Гагаріна, 14/1"
      }
    ]
  },
  {
    "town": "Кагарлык",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 11"
      }
    ]
  },
  {
    "town": "Казатин",
    "servicesCenters": [
      {
        "address": "вул. Героїв Майдану, 17б"
      }
    ]
  },
  {
    "town": "Калиновка",
    "servicesCenters": [
      {
        "address": " вул. Промислова, 1"
      }
    ]
  },
  {
    "town": "Калуш",
    "servicesCenters": [
      {
        "address": "пр. Лесі Українки, 1"
      }
    ]
  },
  {
    "town": "Каменец-Подольский",
    "servicesCenters": [
      {
        "address": " вул. Огієнка, 41"
      },
      {
        "address": " вул. Соборна, 29"
      }
    ]
  },
  {
    "town": "Каменка-Бугская",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 65"
      }
    ]
  },
  {
    "town": "Камень-Каширский",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 16/2а"
      }
    ]
  },
  {
    "town": "Каменское (Днепродзержинск)",
    "servicesCenters": [
      {
        "address": "пр. Свободи (Леніна), 40"
      },
      {
        "address": "пр. Свободи (Леніна), 55"
      },
      {
        "address": " бул. Будівельників, 7"
      }
    ]
  },
  {
    "town": "Канев",
    "servicesCenters": [
      {
        "address": "вул. Кошового, 2"
      }
    ]
  },
  {
    "town": "Киев",
    "servicesCenters": [
      {
        "address": "пр. Победы, 86а"
      },
      {
        "address": " вул. Антоновича (Горького), 176, ТРЦ \"Ocean Plaza\""
      },
      {
        "address": "вул. Ізюмська, 1"
      },
      {
        "address": "Киев вул. Ак. Глушкова, 13б, ТРЦ \"Магеллан\""
      },
      {
        "address": "вул. Андрія Малишка, 4/2"
      },
      {
        "address": "вул. Антоновича, 176, ТРЦ Ocean Plaza"
      },
      {
        "address": "пр. Бажана, (ст. метро \"Позняки\")"
      },
      {
        "address": "пр. Бажана, 8; супермаркет Новус"
      },
      {
        "address": "вул. Берковецька, 6, ТЦ Ашан"
      },
      {
        "address": "Бессарабська, 2"
      },
      {
        "address": " вул. Будівельників, 40, ТРЦ DOMA center"
      },
      {
        "address": " пр. Відрадний, 16/50"
      },
      {
        "address": "вул. Велика Васильківська 72, ТРЦ Олімпійский"
      },
      {
        "address": " вул. Велика Васильківська, 63 а"
      },
      {
        "address": "пр. Возз'єднання, 2/1а, ТЦ Дарниця "
      },
      {
        "address": "пр. Генерала Ватутіна, 2т, ТРЦ Sky Mall"
      },
      {
        "address": "вул. Гришка, 3а ТРЦ Аладдін"
      },
      {
        "address": "вул. Дегтярівська, 53а"
      },
      {
        "address": "вул. Кіото, 8"
      },
      {
        "address": "вул. Кирилівська (Фрунзе), 166, ТЦ \"Куренівський\""
      },
      {
        "address": "пр. Леся Курбаса, 6г"
      },
      {
        "address": "вул. Лугова, 12 ТРЦ Караван"
      },
      {
        "address": "вул. Лятошинського, 14"
      },
      {
        "address": "вул. Мельникова, 1Б"
      },
      {
        "address": "вул. Миколи Лаврухіна, 4 ТЦ Район"
      },
      {
        "address": "пр. Степана Бандеры, 23 ТЦ Городок"
      },
      {
        "address": "пр. Степана Бандеры,15а, ТЦ Ашан Петрівка"
      },
      {
        "address": "пр. Степана Бандери, 10"
      },
      {
        "address": "вул. Попудренка/вул. Мурманська, 6"
      },
      {
        "address": "пр. Оболонський, 1б, ТРЦ \"Dream Town\" (1 лінія)"
      },
      {
        "address": "пр. Перемоги, 108/1"
      },
      {
        "address": "бул. Перова, 36, ТРЦ \"Квадрат\""
      },
      {
        "address": "вул. Старовокзальна, 9а "
      },
      {
        "address": "вул. Сагайдачного, 29а"
      },
      {
        "address": " пров. Політехнічний, 1/33"
      },
      {
        "address": " Хрещатик, 13"
      },
      {
        "address": "вул. Полярна, 2, літ. А"
      },
      {
        "address": "пл. Печерська, 1"
      },
      {
        "address": "вул. Срібнокільська, 20, метро \"Осокорки\""
      },
      {
        "address": "пр. Повітрофлотський, 50/2"
      },
      {
        "address": "вул. Тимошенка, 14"
      },
      {
        "address": "пр.Перемоги, 86 а"
      }
    ]
  },
  {
    "town": "Килия",
    "servicesCenters": [
      {
        "address": " вул. Миру (був. Леніна), 25"
      }
    ]
  },
  {
    "town": "Ковель",
    "servicesCenters": [
      {
        "address": " бул. Лесі Українки, 11/11"
      },
      {
        "address": "вул. Незалежності, 120"
      }
    ]
  },
  {
    "town": "Коломыя",
    "servicesCenters": [
      {
        "address": "пл. Відродження, 3"
      }
    ]
  },
  {
    "town": "Конотоп",
    "servicesCenters": [
      {
        "address": "пр. Миру, 9"
      },
      {
        "address": "вул. Свободи, 14"
      }
    ]
  },
  {
    "town": "Константиновка",
    "servicesCenters": [
      {
        "address": " вул. Ціолковського, 2в"
      }
    ]
  },
  {
    "town": "Коростень",
    "servicesCenters": [
      {
        "address": "Базарна Площа, 18а"
      }
    ]
  },
  {
    "town": "Коростышев",
    "servicesCenters": [
      {
        "address": "вул. Київська (був. Більшовицька), 66, ТЦ \"Фуршет\""
      }
    ]
  },
  {
    "town": "Корсунь-Шевченковский",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 24"
      }
    ]
  },
  {
    "town": "Косов",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 44/1"
      }
    ]
  },
  {
    "town": "Костополь",
    "servicesCenters": [
      {
        "address": "вул. Грушевського, 22"
      }
    ]
  },
  {
    "town": "Краматорск",
    "servicesCenters": [
      {
        "address": "вул. Палацова, 9"
      }
    ]
  },
  {
    "town": "Красилов",
    "servicesCenters": [
      {
        "address": "пр. Ціолковського, 6"
      }
    ]
  },
  {
    "town": "Красноград",
    "servicesCenters": [
      {
        "address": "вул. Полтавська, 91"
      }
    ]
  },
  {
    "town": "Красноильск",
    "servicesCenters": [
      {
        "address": "вул. Штефана чел Маре, 4"
      }
    ]
  },
  {
    "town": "Кременец",
    "servicesCenters": [
      {
        "address": "вул. Медова, 5"
      }
    ]
  },
  {
    "town": "Кременчуг",
    "servicesCenters": [
      {
        "address": " вул. Соборна (був. Леніна), 40/2"
      },
      {
        "address": " вул. Лесі Українки (був. 50-років Жовтня), 39"
      }
    ]
  },
  {
    "town": "Кривой Рог",
    "servicesCenters": [
      {
        "address": "ул. В. Великого, 28"
      },
      {
        "address": "вул. Волгоградська, 9"
      },
      {
        "address": "вул. Ватутіна, 29 "
      },
      {
        "address": " вул. Лермонтова, 2"
      },
      {
        "address": " вул. Криворіжсталі (Орджонікідзе), 11а"
      },
      {
        "address": "пр. Соборності (Косіора), 29б, ТЦ Аркада"
      },
      {
        "address": "пр. Південний, 32"
      },
      {
        "address": "вул. Доватора, 2"
      },
      {
        "address": "пр. Перемоги, 25 (р-н Інгулець)"
      },
      {
        "address": "пр. 200-річчя Кривого Рогу, 14а"
      }
    ]
  },
  {
    "town": "Кропивницкий (Кировоград)",
    "servicesCenters": [
      {
        "address": "вул. Велика Перспективна (Карла Маркса), 15/7"
      },
      {
        "address": "вул. Преображенська, 7а (Центральний ринок)"
      }
    ]
  },
  {
    "town": "Крыжополь",
    "servicesCenters": [
      {
        "address": "вул. Героїв України (був. К. Маркса), 72"
      }
    ]
  },
  {
    "town": "Ладыжин",
    "servicesCenters": [
      {
        "address": "вул. Будівельників, 17а"
      }
    ]
  },
  {
    "town": "Львов",
    "servicesCenters": [
      {
        "address": "вул. Валова, 11"
      },
      {
        "address": "вул. Городоцька, 139"
      },
      {
        "address": "вул. Зелена, 3"
      },
      {
        "address": "вул. Шпитальна, 7"
      },
      {
        "address": "вул. Зубрівська, 27"
      },
      {
        "address": "вул. Беринди, 1"
      },
      {
        "address": "вул. Щирецька, 36"
      },
      {
        "address": "вул. Стрийська, ТРЦ \"King Cross Leopolis\""
      },
      {
        "address": "вул. Городоцька, 159"
      },
      {
        "address": "вул. Червоної Калини, 113"
      },
      {
        "address": "пр. Свободи, 11"
      },
      {
        "address": "вул. Княгині Ольги, 106"
      },
      {
        "address": "пр. В. Чорновола, 93"
      },
      {
        "address": "пр. Шевченка, 8"
      }
    ]
  },
  {
    "town": "Лозовая",
    "servicesCenters": [
      {
        "address": "вул. Володарського, 7"
      }
    ]
  },
  {
    "town": "Лубны",
    "servicesCenters": [
      {
        "address": " пр.Володимирський (Радянська), 40"
      }
    ]
  },
  {
    "town": "Лугины",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 2"
      }
    ]
  },
  {
    "town": "Луцк",
    "servicesCenters": [
      {
        "address": " вул. Лесі Українки, 56"
      },
      {
        "address": "вул. Дубнівська, 12"
      },
      {
        "address": " пр. Волі, 4"
      }
    ]
  },
  {
    "town": "Люботин",
    "servicesCenters": [
      {
        "address": "ул. Шевченко, 100"
      }
    ]
  },
  {
    "town": "Магдалиновка",
    "servicesCenters": [
      {
        "address": "ул. Центральная, 14"
      }
    ]
  },
  {
    "town": "Макаров",
    "servicesCenters": [
      {
        "address": "вул. Фрунзе, 31"
      }
    ]
  },
  {
    "town": "Малин",
    "servicesCenters": [
      {
        "address": "вул. Грушевського, 43"
      }
    ]
  },
  {
    "town": "Маневичи",
    "servicesCenters": [
      {
        "address": "ул.100-летия Маневич, 16"
      }
    ]
  },
  {
    "town": "Маньковка",
    "servicesCenters": [
      {
        "address": "вул. Соборна (був. Леніна), 41"
      }
    ]
  },
  {
    "town": "Мариуполь",
    "servicesCenters": [
      {
        "address": "пр. Миру (був. Леніна), 72а"
      },
      {
        "address": "пр. Металургів, 64"
      },
      {
        "address": "пр. Перемоги, 36/23"
      },
      {
        "address": "вул. Будівельників, 125а"
      }
    ]
  },
  {
    "town": "Мелитополь",
    "servicesCenters": [
      {
        "address": "вул. Б. Хмельницького 24/1"
      }
    ]
  },
  {
    "town": "Миргород",
    "servicesCenters": [
      {
        "address": "вул. Гоголя, 129/2"
      }
    ]
  },
  {
    "town": "Мироновка",
    "servicesCenters": [
      {
        "address": "вул. Соборності (був. Ленiна), 77а"
      }
    ]
  },
  {
    "town": "Могилев-Подольский",
    "servicesCenters": [
      {
        "address": "вул. Стависька, 24"
      }
    ]
  },
  {
    "town": "Мостиска",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 9"
      }
    ]
  },
  {
    "town": "Мукачево",
    "servicesCenters": [
      {
        "address": "вул. Достоєвського, 9/9"
      },
      {
        "address": "вул. Масарика Томаша (Матросова), 15 ТЦ \"Гід\" пав. 3017"
      },
      {
        "address": "вул. Беляєва, 11/1"
      }
    ]
  },
  {
    "town": "Надвирна",
    "servicesCenters": [
      {
        "address": "вул. Чорновола, 4"
      }
    ]
  },
  {
    "town": "Нежин",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 21а"
      }
    ]
  },
  {
    "town": "Нетешин",
    "servicesCenters": [
      {
        "address": "пр. Незалежності, 9"
      }
    ]
  },
  {
    "town": "Николаев",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 2"
      },
      {
        "address": "пр. Центральний (був. Леніна), 98 ТРЦ Сіті Центр"
      },
      {
        "address": "пр. Миру, 2"
      }
    ]
  },
  {
    "town": "Никополь",
    "servicesCenters": [
      {
        "address": "вул. Трубників, 24"
      }
    ]
  },
  {
    "town": "Новая Каховка",
    "servicesCenters": [
      {
        "address": "вул. Ентузіастів, 4"
      }
    ]
  },
  {
    "town": "Нововолынск",
    "servicesCenters": [
      {
        "address": "бул. Шевченка, 4"
      }
    ]
  },
  {
    "town": "Новоград-Волынский",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 26"
      }
    ]
  },
  {
    "town": "Новоукраинка",
    "servicesCenters": [
      {
        "address": "вул. Соборна (Леніна), 53"
      }
    ]
  },
  {
    "town": "Новояворовск",
    "servicesCenters": [
      {
        "address": "вул. С. Бандери, 18"
      }
    ]
  },
  {
    "town": "Обухов",
    "servicesCenters": [
      {
        "address": "вул. Київська, 121а"
      }
    ]
  },
  {
    "town": "Овруч",
    "servicesCenters": [
      {
        "address": "вул. Тараса Шевченка (був. Радянська), 46"
      }
    ]
  },
  {
    "town": "Одесса",
    "servicesCenters": [
      {
        "address": "пр. Адміральський, 37"
      },
      {
        "address": "вул. Ак. Корольова, 76/1"
      },
      {
        "address": "пр. Академіка Глушка, 14"
      },
      {
        "address": "вул. Генерала Бочарова, 40б"
      },
      {
        "address": "пл. Грецька, 3/4, ТЦ Афіна"
      },
      {
        "address": "пр. Добровольського, 137, ТРЦ \"Мой Дом\""
      },
      {
        "address": "вул. Космонавтів, 32"
      },
      {
        "address": "вул. Осипова, 25"
      },
      {
        "address": "Пантелеймонівська, 74"
      },
      {
        "address": "вул. Пантелеймонівська, 25 ТРК Привоз"
      },
      {
        "address": "пл. Старосінна, 2"
      },
      {
        "address": "с. Фонтанка, ТРЦ Рив'єра"
      }
    ]
  },
  {
    "town": "Одесcа",
    "servicesCenters": [
      {
        "address": "пр. Небесної Сотні (Маршала Жукова), 2, ТЦ Сіті Центр"
      }
    ]
  },
  {
    "town": "Одеcса",
    "servicesCenters": [
      {
        "address": "вул. В. Арнаутська, 80"
      }
    ]
  },
  {
    "town": "Олевск",
    "servicesCenters": [
      {
        "address": "вул. Привокзальна, 8"
      }
    ]
  },
  {
    "town": "Орджоникидзе",
    "servicesCenters": [
      {
        "address": "вул. Центральна (був. Калініна), 46"
      }
    ]
  },
  {
    "town": "Острог",
    "servicesCenters": [
      {
        "address": "пр. Незалежності, 77а"
      }
    ]
  },
  {
    "town": "Очаков",
    "servicesCenters": [
      {
        "address": "вул. Лоцманська (був. Леніна), 28/1"
      }
    ]
  },
  {
    "town": "Первомайск",
    "servicesCenters": [
      {
        "address": "вул. Грушевського, 19"
      }
    ]
  },
  {
    "town": "Первомайский",
    "servicesCenters": [
      {
        "address": "Мікро р-н 1/2, буд. 11"
      }
    ]
  },
  {
    "town": "Переяслав-Хмельницкий",
    "servicesCenters": [
      {
        "address": "вул. Богдана Хмельницького, б/н"
      }
    ]
  },
  {
    "town": "Подволочиск",
    "servicesCenters": [
      {
        "address": "вул. Д. Галицького, 64"
      }
    ]
  },
  {
    "town": "Покровск",
    "servicesCenters": [
      {
        "address": "вул. Тургенєва, 6"
      }
    ]
  },
  {
    "town": "Полтава",
    "servicesCenters": [
      {
        "address": "вул. Зіньківська, 6/1а ТРЦ Київ"
      },
      {
        "address": "вул. Ковпака, 26 ТРЦ Екватор"
      },
      {
        "address": "вул. Соборності (Жовтнева), 53/1"
      }
    ]
  },
  {
    "town": "Прилуки",
    "servicesCenters": [
      {
        "address": "вул. Київська, 172/1"
      }
    ]
  },
  {
    "town": "Радехов",
    "servicesCenters": [
      {
        "address": "вул. Львівська, 1а/1"
      }
    ]
  },
  {
    "town": "Радомышль",
    "servicesCenters": [
      {
        "address": "вул. Велика Житомирська, 4а"
      }
    ]
  },
  {
    "town": "Ратно",
    "servicesCenters": [
      {
        "address": "вул. Центральна, 39"
      }
    ]
  },
  {
    "town": "Рени",
    "servicesCenters": [
      {
        "address": " вул. Вознесенська (був. Комсомольська), 137"
      }
    ]
  },
  {
    "town": "Ровно",
    "servicesCenters": [
      {
        "address": "вул. Гагаріна, 16, ТРЦ \"Чайка\""
      },
      {
        "address": " вул. Міцкевича, 34"
      },
      {
        "address": "вул. Макарова, 23 ТРЦ \"Екватор\""
      },
      {
        "address": "вул. Соборна, 190 В"
      },
      {
        "address": "вул. Соборна, 61"
      }
    ]
  },
  {
    "town": "Рогатин",
    "servicesCenters": [
      {
        "address": "вул. Галицька, 52-б"
      }
    ]
  },
  {
    "town": "Рожнятов",
    "servicesCenters": [
      {
        "address": "вул. Єдності, 3а"
      }
    ]
  },
  {
    "town": "Рокитное",
    "servicesCenters": [
      {
        "address": "вул. Кірова, 2б"
      }
    ]
  },
  {
    "town": "Ромны",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 1"
      }
    ]
  },
  {
    "town": "Самбор",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 36"
      }
    ]
  },
  {
    "town": "Сарны",
    "servicesCenters": [
      {
        "address": "вул. Широка, 37"
      }
    ]
  },
  {
    "town": "Светловодск",
    "servicesCenters": [
      {
        "address": "вул. Героїв України (був. Леніна), 106"
      }
    ]
  },
  {
    "town": "Северодонецк",
    "servicesCenters": [
      {
        "address": "вул. Хіміків, 32"
      }
    ]
  },
  {
    "town": "Синельниково",
    "servicesCenters": [
      {
        "address": "вул. Виконкомівська (торговельний павільйон)"
      }
    ]
  },
  {
    "town": "Сквира",
    "servicesCenters": [
      {
        "address": "вул. Червона площа, 3"
      }
    ]
  },
  {
    "town": "Сколе",
    "servicesCenters": [
      {
        "address": "вул. Д. Галицького, 1"
      }
    ]
  },
  {
    "town": "Славянск",
    "servicesCenters": [
      {
        "address": "вул. Університетська (був. Леніна), 37"
      }
    ]
  },
  {
    "town": "Смела",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 89а"
      }
    ]
  },
  {
    "town": "Сокаль",
    "servicesCenters": [
      {
        "address": "вул. Шептицького, 50"
      }
    ]
  },
  {
    "town": "Сокиряны",
    "servicesCenters": [
      {
        "address": "вул. 28 червня, 7"
      }
    ]
  },
  {
    "town": "Староконстантинов",
    "servicesCenters": [
      {
        "address": "вул. Острозького, 4"
      }
    ]
  },
  {
    "town": "Сторожинец",
    "servicesCenters": [
      {
        "address": "вул. О. Кобилянської, 1"
      }
    ]
  },
  {
    "town": "Стрый",
    "servicesCenters": [
      {
        "address": " м-н Ринок, 5"
      }
    ]
  },
  {
    "town": "Сумы",
    "servicesCenters": [
      {
        "address": " Набережна р. Стрілки, 10"
      },
      {
        "address": "вул. Петропавлівська, 49"
      }
    ]
  },
  {
    "town": "Тальное",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 35"
      }
    ]
  },
  {
    "town": "Тараща",
    "servicesCenters": [
      {
        "address": " вул. Б. Хмельницького, 46"
      }
    ]
  },
  {
    "town": "Теребовля",
    "servicesCenters": [
      {
        "address": "вул. Князя Василька, 95"
      }
    ]
  },
  {
    "town": "Тересва",
    "servicesCenters": [
      {
        "address": "вул. Народна, 126а"
      }
    ]
  },
  {
    "town": "Терново",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 96"
      }
    ]
  },
  {
    "town": "Тернополь",
    "servicesCenters": [
      {
        "address": "вул. Гетьмана Сагайдачного, 3"
      },
      {
        "address": " вул. Руська, 23"
      },
      {
        "address": " вул. Руська, 33"
      },
      {
        "address": "вул. Текстильна, 28 ч, ТРЦ \"Подоляни\""
      },
      {
        "address": " вул. Шептицького, 6"
      }
    ]
  },
  {
    "town": "Тетиев",
    "servicesCenters": [
      {
        "address": "вул. Цвєткова, 2"
      }
    ]
  },
  {
    "town": "Токмак",
    "servicesCenters": [
      {
        "address": "вул. Революційна, 32"
      }
    ]
  },
  {
    "town": "Томаковка",
    "servicesCenters": [
      {
        "address": " вул. Шосейна, 6"
      }
    ]
  },
  {
    "town": "Тульчин",
    "servicesCenters": [
      {
        "address": "вул. Леонтовича (був. Леніна), 39"
      }
    ]
  },
  {
    "town": "Тячев",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 18"
      }
    ]
  },
  {
    "town": "Ужгород",
    "servicesCenters": [
      {
        "address": "пл. Кирила і Мефодія, 1"
      },
      {
        "address": "вул. Корятовича, 12"
      }
    ]
  },
  {
    "town": "Узин",
    "servicesCenters": [
      {
        "address": "вул. Соборна (був.Першотравнева), 6-А, пав. 12"
      }
    ]
  },
  {
    "town": "Ульяновка",
    "servicesCenters": [
      {
        "address": "вул. Героїв України (був. Леніна), 58"
      }
    ]
  },
  {
    "town": "Умань",
    "servicesCenters": [
      {
        "address": "вул. Європейська (був. Леніна), 7/1"
      }
    ]
  },
  {
    "town": "Фастов",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 28"
      }
    ]
  },
  {
    "town": "Харьков",
    "servicesCenters": [
      {
        "address": "вул. 23 Серпня, 30"
      },
      {
        "address": "вул. Академіка Павлова, 321"
      },
      {
        "address": " вул. Валентинівська (Блюхера) 37/128"
      },
      {
        "address": " вул. Валентинівська (був. Блюхера), 21"
      },
      {
        "address": "пр. Гагаріна, 244-А"
      },
      {
        "address": "пр. Гагаріна, 22"
      },
      {
        "address": "вул. Героїв Праці, 7 ТРЦ Караван"
      },
      {
        "address": "вул. Героїв Праці, 9, ТРЦ Дафі"
      },
      {
        "address": "пр. Героїв Сталінграда, 134-3"
      },
      {
        "address": "майдан Захисників України, 1"
      },
      {
        "address": "вул. Конарєва (був. Червоноармійська), 8/10"
      },
      {
        "address": "пл. Конституції, 12"
      },
      {
        "address": "пр. Московський, 256, метро «Мосельського»"
      },
      {
        "address": "пр. Науки (був. Леніна), 9"
      },
      {
        "address": "пр.Олександрійський (був. Косіора), 97"
      },
      {
        "address": "пр. Петра Григоренка (був. Маршала Жукова), 7"
      },
      {
        "address": " вул. Полтавський шлях, 148/2"
      },
      {
        "address": "вул. Різдвяна, 33"
      },
      {
        "address": "вул. Сумська, 72"
      },
      {
        "address": "пр. Ювілейний (був.50-річчя ВЛКСМ), 57а"
      }
    ]
  },
  {
    "town": "Херсон",
    "servicesCenters": [
      {
        "address": "вул. Іллюши Кулика, 122"
      },
      {
        "address": "вул. Залаегерсег, 18"
      },
      {
        "address": "вул. Потьомкінська, 40/10"
      }
    ]
  },
  {
    "town": "Хмельник",
    "servicesCenters": [
      {
        "address": "пр. Свободи (був. вул. Леніна), 22/1"
      }
    ]
  },
  {
    "town": "Хмельницький",
    "servicesCenters": [
      {
        "address": "вул. Героїв Майдану (був. Театральна)"
      },
      {
        "address": "вул. Кам'янецька, 17"
      },
      {
        "address": "вул. Проскурівська, 34"
      }
    ]
  },
  {
    "town": "Хуст",
    "servicesCenters": [
      {
        "address": "вул. Духновича, 2"
      },
      {
        "address": "вул. Карпатської Січі, 32а"
      }
    ]
  },
  {
    "town": "Царичанка",
    "servicesCenters": [
      {
        "address": "вул. Театральна, 16"
      }
    ]
  },
  {
    "town": "Червоноград",
    "servicesCenters": [
      {
        "address": "вул. Шептицького, 12"
      }
    ]
  },
  {
    "town": "Черкассы",
    "servicesCenters": [
      {
        "address": "вул. Смілянська, 36"
      },
      {
        "address": "вул.Шевченка, 385, ТРЦ \"Депот\""
      }
    ]
  },
  {
    "town": "Чернигов",
    "servicesCenters": [
      {
        "address": "вул. 77 Гвардійської Дивізії, 1 в"
      },
      {
        "address": "пр. Перемоги, 95, а-4"
      },
      {
        "address": "вул. Рокосовського, 25"
      }
    ]
  },
  {
    "town": "Чернобай",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 83"
      }
    ]
  },
  {
    "town": "Черновцы",
    "servicesCenters": [
      {
        "address": "пр. Незалежності, 113"
      },
      {
        "address": "пл. Соборна, 9"
      }
    ]
  },
  {
    "town": "Чортков",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 3"
      }
    ]
  },
  {
    "town": "Чугуев",
    "servicesCenters": [
      {
        "address": "вул. Жадановського, 3а"
      }
    ]
  },
  {
    "town": "Чудей",
    "servicesCenters": [
      {
        "address": "вул. Карпатська, 1, ТД \"Прибутковий\""
      }
    ]
  },
  {
    "town": "Шепетовка",
    "servicesCenters": [
      {
        "address": "вул. Героїв Небесної Сотні (був. Карла Маркса), 35"
      }
    ]
  },
  {
    "town": "Шостка",
    "servicesCenters": [
      {
        "address": "вул. Свободи (К.Маркса), 22"
      }
    ]
  },
  {
    "town": "Шпола",
    "servicesCenters": [
      {
        "address": "вул. Соборна (Ленiна), 7"
      }
    ]
  },
  {
    "town": "Южноукраинск",
    "servicesCenters": [
      {
        "address": "просп. Незалежності, (був. Леніна), 22"
      }
    ]
  },
  {
    "town": "Ямполь",
    "servicesCenters": [
      {
        "address": "вул. Свободи (був. Леніна), 140"
      }
    ]
  },
  {
    "town": "Ярмолинцы",
    "servicesCenters": [
      {
        "address": "вул. Хмельницька, 6"
      }
    ]
  },
  {
    "town": "Ясиня",
    "servicesCenters": [
      {
        "address": "вул. Миру, 8"
      }
    ]
  }
]};
