Vue.config.KS_CONFIG = {
	serviceCenters: [
  {
    "town": "Андрушівка",
    "servicesCenters": [
      {
        "address": "вул. Зазулинського, 9а "
      }
    ]
  },
  {
    "town": "Апостолове",
    "servicesCenters": [
      {
        "address": "вул. Ветеранів, 29а "
      }
    ]
  },
  {
    "town": "Балаклія",
    "servicesCenters": [
      {
        "address": "вул. Жовтнева, 37 "
      }
    ]
  },
  {
    "town": "Бар",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 6б "
      }
    ]
  },
  {
    "town": "Бахмач",
    "servicesCenters": [
      {
        "address": "вул. Першотравнева, 41а"
      }
    ]
  },
  {
    "town": "Бахмут",
    "servicesCenters": [
      {
        "address": "вул. Торгова (був. Воровського), 1 "
      }
    ]
  },
  {
    "town": "Бердичів",
    "servicesCenters": [
      {
        "address": "вул. Європейська, 10"
      }
    ]
  },
  {
    "town": "Бердянськ",
    "servicesCenters": [
      {
        "address": "вул. Італійська (був. Дюміна), 48/28 "
      }
    ]
  },
  {
    "town": "Бережани",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 3/2 "
      }
    ]
  },
  {
    "town": "Березне",
    "servicesCenters": [
      {
        "address": "вул. Андріївська, 5"
      }
    ]
  },
  {
    "town": "Бершадь",
    "servicesCenters": [
      {
        "address": "вул. Ярослава Мудрого (був. Леніна), 1а"
      }
    ]
  },
  {
    "town": "Біла Церква",
    "servicesCenters": [
      {
        "address": "вул. Ярослава Мудрого, 21а"
      },
      {
        "address": "бул. Олександрійський (пр.50-річчя Перемоги), 95-97 "
      },
      {
        "address": "вул. Леваневського, 59 "
      }
    ]
  },
  {
    "town": "Білгород-Дністровський",
    "servicesCenters": [
      {
        "address": "вул. Першотравнева, 86/88"
      }
    ]
  },
  {
    "town": "Благовіщенське (Ульяновка)",
    "servicesCenters": [
      {
        "address": "вул. Героїв України (Леніна), 58"
      }
    ]
  },
  {
    "town": "Богодухів",
    "servicesCenters": [
      {
        "address": "вул. Кірова, 44 "
      }
    ]
  },
  {
    "town": "Богуслав",
    "servicesCenters": [
      {
        "address": "вул. І. Франка, 22/1"
      }
    ]
  },
  {
    "town": "Болград",
    "servicesCenters": [
      {
        "address": "пр. Соборний (Леніна), 109 "
      }
    ]
  },
  {
    "town": "Борзна",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 1 "
      }
    ]
  },
  {
    "town": "Бориспіль",
    "servicesCenters": [
      {
        "address": "вул. Київський Шлях, 2/6 ТЦ Аеромолл"
      },
      {
        "address": "Київський шлях, 67 ТРЦ Парк Таун "
      }
    ]
  },
  {
    "town": "Борщів",
    "servicesCenters": [
      {
        "address": "вул. Верхратського,3д"
      }
    ]
  },
  {
    "town": "Боярка",
    "servicesCenters": [
      {
        "address": "вул. Хрещатик, 43"
      }
    ]
  },
  {
    "town": "Бровари",
    "servicesCenters": [
      {
        "address": "бул. Незалежності, 2 "
      }
    ]
  },
  {
    "town": "Броди",
    "servicesCenters": [
      {
        "address": "вул. Золота, 1"
      }
    ]
  },
  {
    "town": "Бурштин",
    "servicesCenters": [
      {
        "address": "вул. Калуська, 5а супермаркет Гулівер "
      }
    ]
  },
  {
    "town": "Бучач",
    "servicesCenters": [
      {
        "address": "вул. Підгаєцька, 6 "
      }
    ]
  },
  {
    "town": "Вараш (Кузнецовськ)",
    "servicesCenters": [
      {
        "address": "мкрн. Вараш, 11/2"
      }
    ]
  },
  {
    "town": "Васильків",
    "servicesCenters": [
      {
        "address": "вул. Грушевського, 8/1"
      }
    ]
  },
  {
    "town": "Васильківка",
    "servicesCenters": [
      {
        "address": "вул. Першотравнева, 201б "
      }
    ]
  },
  {
    "town": "Ватутіне",
    "servicesCenters": [
      {
        "address": " вул. Франка, 1/11"
      }
    ]
  },
  {
    "town": "Велика Новосілка",
    "servicesCenters": [
      {
        "address": "вул. Центральна (був. Жовтнева), 36 "
      }
    ]
  },
  {
    "town": "Великий Бичків",
    "servicesCenters": [
      {
        "address": "вул. Грушевського, 113 а, ТЦ Шафран"
      }
    ]
  },
  {
    "town": "Верховина",
    "servicesCenters": [
      {
        "address": "вул. І. Франка, 1 "
      }
    ]
  },
  {
    "town": "Вижница",
    "servicesCenters": [
      {
        "address": "вул. Пушкіна, 1 "
      }
    ]
  },
  {
    "town": "Виноградів",
    "servicesCenters": [
      {
        "address": " вул. Шевченка, 3 "
      }
    ]
  },
  {
    "town": "Вишгород",
    "servicesCenters": [
      {
        "address": "вул. Мазепи, 1"
      }
    ]
  },
  {
    "town": "Вишневе",
    "servicesCenters": [
      {
        "address": "вул. Європейська (Жовтнева), 31а "
      }
    ]
  },
  {
    "town": "Вільногірськ",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 65к "
      }
    ]
  },
  {
    "town": "Вінниця",
    "servicesCenters": [
      {
        "address": "вул. Келецька, 78в (пр. Юності) ТРЦ Магігранд "
      },
      {
        "address": "вул. Замостянська (50-річчя Перемоги), 27 "
      },
      {
        "address": "вул. Пирогова, 105 "
      },
      {
        "address": " пр. Коцюбинського, 19"
      },
      {
        "address": "вул. Миколи Оводова (Козицького), 51 ТРЦ Sky Park "
      },
      {
        "address": "вул. Соборна, 46"
      }
    ]
  },
  {
    "town": "Вознесенськ",
    "servicesCenters": [
      {
        "address": "вул. Київська (Жовтневої революції), 3а "
      }
    ]
  },
  {
    "town": "Володимир-Волинський",
    "servicesCenters": [
      {
        "address": " вул. Ковельська, 12"
      }
    ]
  },
  {
    "town": "Волочиськ",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 25"
      }
    ]
  },
  {
    "town": "Гайворон",
    "servicesCenters": [
      {
        "address": "вул. Енергетична, 21/26б"
      }
    ]
  },
  {
    "town": "Генічеськ",
    "servicesCenters": [
      {
        "address": " вул. Дружби народів (був. Махарадзе), 64а"
      }
    ]
  },
  {
    "town": "Гнівань",
    "servicesCenters": [
      {
        "address": "вул. Соборна (Леніна), 58 "
      }
    ]
  },
  {
    "town": "Голованівськ",
    "servicesCenters": [
      {
        "address": "вул. Леніна, (навпроти ТЦ Дитячий Світ) "
      }
    ]
  },
  {
    "town": "Горішні Плавні (Комсомольськ)",
    "servicesCenters": [
      {
        "address": "вул. Героїв Дніпра (був. Леніна), 77"
      }
    ]
  },
  {
    "town": "Городня",
    "servicesCenters": [
      {
        "address": "вул. Першого травня, 14а"
      }
    ]
  },
  {
    "town": "Городок",
    "servicesCenters": [
      {
        "address": "вул. Перемишльська, 5в"
      }
    ]
  },
  {
    "town": "Горохів",
    "servicesCenters": [
      {
        "address": "вул.Шевченко, 21"
      }
    ]
  },
  {
    "town": "Дніпро",
    "servicesCenters": [
      {
        "address": " вул. Тітова, 11"
      },
      {
        "address": "пр. Героїв, 1к"
      },
      {
        "address": "вул. Глінки, 2 ТРЦ Мост Сіті"
      },
      {
        "address": "пр. Слобожанський (кол. ім. Газети \"Правда\"), 87"
      },
      {
        "address": "пр. Дмитра Яворницького (кол. К.Маркса), 65 "
      },
      {
        "address": "бул. Зоряний, 1а ТРЦ Дафі"
      },
      {
        "address": "пр. Дмитра Яворницького (був. К.Маркса), 113"
      },
      {
        "address": "пр. Богдана Хмельницкого (Героїв Сталінграда), 118д ТРЦ Терра "
      },
      {
        "address": " вул. Нижньодніпровська, 17 ТРЦ Караван"
      },
      {
        "address": "вул. Тітова, 36 ТРЦ Appolo "
      },
      {
        "address": "вул.Д.Яворницкого,52 ЦУМ "
      },
      {
        "address": "вул. Пастера, 6а"
      }
    ]
  },
  {
    "town": "Долина",
    "servicesCenters": [
      {
        "address": "пр. Незалежності, 8а "
      }
    ]
  },
  {
    "town": "Долинська",
    "servicesCenters": [
      {
        "address": "вул. Центральна (Радянська), 144/10"
      }
    ]
  },
  {
    "town": "Драбів",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 32"
      }
    ]
  },
  {
    "town": "Дрогобич",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 6"
      }
    ]
  },
  {
    "town": "Дубно",
    "servicesCenters": [
      {
        "address": "вул. Д. Галицького, 4"
      }
    ]
  },
  {
    "town": "Дубове",
    "servicesCenters": [
      {
        "address": "вул. Гагаріна, 13а "
      }
    ]
  },
  {
    "town": "Дунаївці",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 57"
      }
    ]
  },
  {
    "town": "Енергодар",
    "servicesCenters": [
      {
        "address": "вул. Молодіжна (Комсомольська), 43/1"
      }
    ]
  },
  {
    "town": "Житомир",
    "servicesCenters": [
      {
        "address": "вул. Київська, 64"
      },
      {
        "address": "вул. Київська, 1"
      },
      {
        "address": "пл.Житній ринок,1, ТЦ Житній"
      },
      {
        "address": "вул. Київська, 39 "
      },
      {
        "address": "вул. Київська, 77, ТЦ Глобал UA "
      }
    ]
  },
  {
    "town": "Жмеринка",
    "servicesCenters": [
      {
        "address": "вул. Пушкіна, 9 "
      }
    ]
  },
  {
    "town": "Жовтi Води",
    "servicesCenters": [
      {
        "address": "вул. Кропоткіна, 39/37"
      }
    ]
  },
  {
    "town": "Запоріжжя",
    "servicesCenters": [
      {
        "address": "вул. Задніпровська, 9-а "
      },
      {
        "address": "вул. Ладозька, 4"
      },
      {
        "address": "пр. Соборний, 147 ТРЦ Україна "
      },
      {
        "address": "пр. Соборний (був. Леніна), 179 "
      },
      {
        "address": "бул. Центральний, 3"
      },
      {
        "address": "вул. Запорізька, 1 Б ТРЦ Сіті Молл "
      },
      {
        "address": "пр. Соборний, 87з "
      },
      {
        "address": "пр.Соборний (Леніна),42 "
      }
    ]
  },
  {
    "town": "Збараж",
    "servicesCenters": [
      {
        "address": "вул І. Франка, 18"
      }
    ]
  },
  {
    "town": "Звенигородка",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 66б"
      }
    ]
  },
  {
    "town": "Здолбунів",
    "servicesCenters": [
      {
        "address": "вул. Б. Хмельницького, 27б"
      }
    ]
  },
  {
    "town": "Знам'янка",
    "servicesCenters": [
      {
        "address": " вул. Калініна, 118 "
      }
    ]
  },
  {
    "town": "Золотоноша",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 96"
      }
    ]
  },
  {
    "town": "Золочів",
    "servicesCenters": [
      {
        "address": "пл. Вічева, 1"
      }
    ]
  },
  {
    "town": "Іванків",
    "servicesCenters": [
      {
        "address": "вул. Київська, 26"
      }
    ]
  },
  {
    "town": "Івано-Франківськ",
    "servicesCenters": [
      {
        "address": "вул. Галицька, 9"
      },
      {
        "address": " вул. Січових Стрільців, 30"
      },
      {
        "address": " вул. Миколайчука, 2, ТЦ Арсен"
      },
      {
        "address": "вул. Шашкевича, 7 "
      }
    ]
  },
  {
    "town": "Ізмаїл",
    "servicesCenters": [
      {
        "address": "вул. Болградська, 80"
      },
      {
        "address": "пр. Суворова, 37 "
      }
    ]
  },
  {
    "town": "Ізюм",
    "servicesCenters": [
      {
        "address": "вул. Гагаріна, 9б"
      }
    ]
  },
  {
    "town": "Ізяслав",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 19 "
      }
    ]
  },
  {
    "town": "Іллінці",
    "servicesCenters": [
      {
        "address": "вул.Європейська (К. Маркса), 15"
      }
    ]
  },
  {
    "town": "Ірпінь",
    "servicesCenters": [
      {
        "address": "вул. Центральна, 1 "
      }
    ]
  },
  {
    "town": "Іршава",
    "servicesCenters": [
      {
        "address": "вул. Гагаріна, 14/1"
      }
    ]
  },
  {
    "town": "Кагарлик",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 11 "
      }
    ]
  },
  {
    "town": "Калинівка",
    "servicesCenters": [
      {
        "address": "вул. Промислова, 1"
      }
    ]
  },
  {
    "town": "Калуш",
    "servicesCenters": [
      {
        "address": "пр. Лесі Українки, 1"
      }
    ]
  },
  {
    "town": "Камінь-Каширський",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 16/2а"
      }
    ]
  },
  {
    "town": "Кам’янець-Подільский",
    "servicesCenters": [
      {
        "address": "вул. Огієнка, 41 "
      },
      {
        "address": "вул. Соборна, 29"
      }
    ]
  },
  {
    "town": "Кам`янка-Бузька",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 65 "
      }
    ]
  },
  {
    "town": "Кам`янське (Дніпродзержинськ)",
    "servicesCenters": [
      {
        "address": "пр. Свободи (Леніна), 40"
      },
      {
        "address": "пр. Свободи (Леніна), 55 "
      },
      {
        "address": "бул. Будівельників, 7"
      }
    ]
  },
  {
    "town": "Канів",
    "servicesCenters": [
      {
        "address": "вул. Кошового, 2 "
      }
    ]
  },
  {
    "town": "Київ",
    "servicesCenters": [
      {
        "address": "пр. Перемоги, 86а"
      },
      {
        "address": "вул. Антоновича (Горького), 176, ТРЦ \"Ocean Plaza\""
      },
      {
        "address": "вул. Ізюмська, 1 "
      },
      {
        "address": "вул. Ак. Глушкова, 13б, ТРЦ \"Магеллан\""
      },
      {
        "address": "вул. Андрія Малишка, 4/2"
      },
      {
        "address": "вул. Антоновича, 176, ТРЦ Ocean Plaza"
      },
      {
        "address": "пр. Бажана, (ст. метро \"Позняки\") "
      },
      {
        "address": "пр. Бажана, 8; супермаркет Новус"
      },
      {
        "address": "вул. Берковецька, 6, ТЦ Ашан "
      },
      {
        "address": "Бессарабська, 2"
      },
      {
        "address": "вул. Будівельників, 40, ТРЦ DOMA center"
      },
      {
        "address": "пр. Відрадний, 16/50"
      },
      {
        "address": "вул. Велика Васильківська 72, ТРЦ Олімпійский"
      },
      {
        "address": "вул. Велика Васильківська, 63 а "
      },
      {
        "address": "пр. Возз'єднання, 2/1а, ТЦ Дарниця"
      },
      {
        "address": "пр. Генерала Ватутіна, 2т, ТРЦ Sky Mall"
      },
      {
        "address": "вул. Гришка, 3а ТРЦ Аладдін"
      },
      {
        "address": " вул. Дегтярівська, 53а"
      },
      {
        "address": "вул. Кіото, 8"
      },
      {
        "address": "вул. Кирилівська (Фрунзе), 166, ТЦ \"Куренівський\" "
      },
      {
        "address": "пр. Леся Курбаса, 6г "
      },
      {
        "address": "вул. Лугова, 12 ТРЦ Караван "
      },
      {
        "address": " вул. Лятошинського, 14 "
      },
      {
        "address": "вул. Мельникова, 1Б "
      },
      {
        "address": "вул. Миколи Лаврухіна, 4 ТЦ Район "
      },
      {
        "address": "пр. Степана Бандери, 23 ТЦ Городок"
      },
      {
        "address": "пр. Степана Бандери,15а, ТЦ Ашан Петрівка"
      },
      {
        "address": "пр. Степана Бандери, 10"
      },
      {
        "address": " вул. Попудренка/вул. Мурманська, 6 ТРК Базар"
      },
      {
        "address": "пр. Оболонський, 1б, ТРЦ \"Dream Town\" (1 лінія)"
      },
      {
        "address": "пр. Перемоги, 108/1"
      },
      {
        "address": "бул. Перова, 36, ТРЦ \"Квадрат\""
      },
      {
        "address": "вул. Старовокзальна, 9а"
      },
      {
        "address": "вул. Сагайдачного, 29а "
      },
      {
        "address": "пров. Політехнічний, 1/33"
      },
      {
        "address": "Хрещатик, 13 (uk)"
      },
      {
        "address": "вул. Полярна, 2, літ. А "
      },
      {
        "address": "пл. Печерська, 1 "
      },
      {
        "address": "вул. Срібнокільська, 20, метро \"Осокорки\" "
      },
      {
        "address": "пр. Повітрофлотський, 50/2"
      },
      {
        "address": " вул. Тимошенка, 14"
      },
      {
        "address": "пр.Перемоги, 86 а"
      }
    ]
  },
  {
    "town": "Кілія",
    "servicesCenters": [
      {
        "address": " вул. Миру (був. Леніна), 25"
      }
    ]
  },
  {
    "town": "Ковель",
    "servicesCenters": [
      {
        "address": "бул. Лесі Українки, 11/11"
      },
      {
        "address": "вул. Незалежності, 120 "
      }
    ]
  },
  {
    "town": "Козятин",
    "servicesCenters": [
      {
        "address": "вул. Героїв Майдану, 17б "
      }
    ]
  },
  {
    "town": "Коломия",
    "servicesCenters": [
      {
        "address": " пл. Відродження, 3 "
      }
    ]
  },
  {
    "town": "Конотоп",
    "servicesCenters": [
      {
        "address": "пр. Миру, 9 "
      },
      {
        "address": "вул. Свободи, 14 "
      }
    ]
  },
  {
    "town": "Коростень",
    "servicesCenters": [
      {
        "address": "Базарна Площа, 18а"
      }
    ]
  },
  {
    "town": "Коростишів",
    "servicesCenters": [
      {
        "address": "вул. Київська (був. Більшовицька), 66, ТЦ \"Фуршет\" "
      }
    ]
  },
  {
    "town": "Корсунь-Шевченківський",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 24 ",
        "onrepair": "На ремонті"
      }
    ]
  },
  {
    "town": "Косів",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 44/1"
      }
    ]
  },
  {
    "town": "Костопіль",
    "servicesCenters": [
      {
        "address": "вул. Грушевського, 22 "
      }
    ]
  },
  {
    "town": "Костянтинівка",
    "servicesCenters": [
      {
        "address": "вул. Ціолковського, 2в"
      }
    ]
  },
  {
    "town": "Краматорськ",
    "servicesCenters": [
      {
        "address": "вул. Палацова, 9"
      }
    ]
  },
  {
    "town": "Красилів",
    "servicesCenters": [
      {
        "address": "пр. Ціолковського, 6 "
      }
    ]
  },
  {
    "town": "Красноград",
    "servicesCenters": [
      {
        "address": "вул. Полтавська, 91"
      }
    ]
  },
  {
    "town": "Красноїльськ",
    "servicesCenters": [
      {
        "address": " вул. Штефана чел Маре, 4"
      }
    ]
  },
  {
    "town": "Кременець",
    "servicesCenters": [
      {
        "address": "вул. Медова, 5"
      }
    ]
  },
  {
    "town": "Кременчук",
    "servicesCenters": [
      {
        "address": "вул. Соборна (був. Леніна), 40/2"
      },
      {
        "address": "вул. Лесі Українки (був. 50-років Жовтня), 39"
      }
    ]
  },
  {
    "town": "Кривий Ріг",
    "servicesCenters": [
      {
        "address": "вул. В. Великого, 28"
      },
      {
        "address": "вул. Волгоградська, 9 "
      },
      {
        "address": "вул. Ватутіна, 29 "
      },
      {
        "address": "вул. Лермонтова, 2"
      },
      {
        "address": "вул. Криворіжсталі (Орджонікідзе), 11а "
      },
      {
        "address": "пр. Соборності (Косіора), 29б, ТЦ Аркада"
      },
      {
        "address": "пр. Південний, 32 "
      },
      {
        "address": " вул. Доватора, 2 "
      },
      {
        "address": "пр. Перемоги, 25 (р-н Інгулець)"
      },
      {
        "address": "пр. 200-річчя Кривого Рогу, 14а"
      }
    ]
  },
  {
    "town": "Крижопіль",
    "servicesCenters": [
      {
        "address": "вул. Героїв України (був. К. Маркса), 72"
      }
    ]
  },
  {
    "town": "Кропивницький (Кіровоград)",
    "servicesCenters": [
      {
        "address": " вул. Велика Перспективна (Карла Маркса), 15/7 "
      },
      {
        "address": "вул. Преображенська, 7а (Центральний ринок) "
      }
    ]
  },
  {
    "town": "Ладижин",
    "servicesCenters": [
      {
        "address": "вул. Будівельників, 17а "
      }
    ]
  },
  {
    "town": "Львів",
    "servicesCenters": [
      {
        "address": "вул. Валова, 11"
      },
      {
        "address": "вул. Городоцька, 139"
      },
      {
        "address": "вул. Зелена, 3 "
      },
      {
        "address": "вул. Шпитальна, 7 "
      },
      {
        "address": " вул. Зубрівська, 27 "
      },
      {
        "address": "вул. Беринди, 1 "
      },
      {
        "address": " вул. Щирецька, 36 "
      },
      {
        "address": "вул. Стрийська, ТРЦ \"King Cross Leopolis\""
      },
      {
        "address": "вул. Городоцька, 159"
      },
      {
        "address": "вул. Червоної Калини, 113"
      },
      {
        "address": "пр. Свободи, 11 "
      },
      {
        "address": "вул. Княгині Ольги, 106 "
      },
      {
        "address": "пр. В. Чорновола, 93"
      },
      {
        "address": "пр. Шевченка, 8"
      }
    ]
  },
  {
    "town": "Лозова",
    "servicesCenters": [
      {
        "address": "вул. Володарського, 7 "
      }
    ]
  },
  {
    "town": "Лубни",
    "servicesCenters": [
      {
        "address": "пр.Володимирський (Радянська), 40 "
      }
    ]
  },
  {
    "town": "Лугини",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 2 "
      }
    ]
  },
  {
    "town": "Луцьк",
    "servicesCenters": [
      {
        "address": "вул. Лесі Українки, 56"
      },
      {
        "address": "вул. Дубнівська, 12"
      },
      {
        "address": "пр. Волі, 4"
      }
    ]
  },
  {
    "town": "Люботин",
    "servicesCenters": [
      {
        "address": "вул. Шевченко, 100"
      }
    ]
  },
  {
    "town": "Магдалинівка",
    "servicesCenters": [
      {
        "address": "вул. Центральна, 14"
      }
    ]
  },
  {
    "town": "Макарів",
    "servicesCenters": [
      {
        "address": " вул. Фрунзе, 31"
      }
    ]
  },
  {
    "town": "Малин",
    "servicesCenters": [
      {
        "address": "вул. Грушевського, 43"
      }
    ]
  },
  {
    "town": "Маневичі",
    "servicesCenters": [
      {
        "address": "вул.100-річчя Маневич, 16"
      }
    ]
  },
  {
    "town": "Маньківка",
    "servicesCenters": [
      {
        "address": "вул. Соборна (був. Леніна), 41 "
      }
    ]
  },
  {
    "town": "Маріуполь",
    "servicesCenters": [
      {
        "address": "пр. Миру (був. Леніна), 72а"
      },
      {
        "address": "пр. Металургів, 64"
      },
      {
        "address": "пр. Перемоги, 36/23"
      }
    ]
  },
  {
    "town": "Мелітополь",
    "servicesCenters": [
      {
        "address": "вул. Б. Хмельницького 24/1"
      }
    ]
  },
  {
    "town": "Миколаїв",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 2 "
      },
      {
        "address": "пр. Центральний (був. Леніна), 98 ТРЦ Сіті Центр "
      },
      {
        "address": "пр. Миру, 2"
      }
    ]
  },
  {
    "town": "Миргород",
    "servicesCenters": [
      {
        "address": " вул. Гоголя, 129/2"
      }
    ]
  },
  {
    "town": "Миронiвка",
    "servicesCenters": [
      {
        "address": "вул. Соборності (був. Ленiна), 77а"
      }
    ]
  },
  {
    "town": "Могилів-Подільський",
    "servicesCenters": [
      {
        "address": "вул. Стависька, 24"
      }
    ]
  },
  {
    "town": "Мостиська",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 9"
      }
    ]
  },
  {
    "town": "Мукачеве",
    "servicesCenters": [
      {
        "address": "вул. Достоєвського, 9/9"
      },
      {
        "address": "вул. Масарика Томаша (Матросова), 15 ТЦ \"Гід\" пав. 3017 "
      }
    ]
  },
  {
    "town": "Мукачевe",
    "servicesCenters": [
      {
        "address": "вул. Беляєва, 11/1 "
      }
    ]
  },
  {
    "town": "Надвірна",
    "servicesCenters": [
      {
        "address": "вул. Чорновола, 4"
      }
    ]
  },
  {
    "town": "Нетішин",
    "servicesCenters": [
      {
        "address": "пр. Незалежності, 9 "
      }
    ]
  },
  {
    "town": "Ніжин",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 21а"
      }
    ]
  },
  {
    "town": "Нікополь",
    "servicesCenters": [
      {
        "address": "вул. Трубників, 24"
      }
    ]
  },
  {
    "town": "Нова Каховка",
    "servicesCenters": [
      {
        "address": "вул. Ентузіастів, 4"
      }
    ]
  },
  {
    "town": "Нововолинськ",
    "servicesCenters": [
      {
        "address": "бул. Шевченка, 4 "
      }
    ]
  },
  {
    "town": "Новоград-Волинський",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 26"
      }
    ]
  },
  {
    "town": "Новоукраїнка",
    "servicesCenters": [
      {
        "address": "вул. Соборна (Леніна), 53"
      }
    ]
  },
  {
    "town": "Новояворівськ",
    "servicesCenters": [
      {
        "address": "вул. С. Бандери, 18"
      }
    ]
  },
  {
    "town": "Обухів",
    "servicesCenters": [
      {
        "address": "вул. Київська, 121а "
      }
    ]
  },
  {
    "town": "Овруч",
    "servicesCenters": [
      {
        "address": "вул. Тараса Шевченка (був. Радянська), 46 "
      }
    ]
  },
  {
    "town": "Одеса",
    "servicesCenters": [
      {
        "address": "пр. Адміральський, 37"
      },
      {
        "address": "вул. Ак. Корольова, 76/1 "
      },
      {
        "address": "пр. Академіка Глушка, 14"
      },
      {
        "address": "вул. В. Арнаутська, 80"
      },
      {
        "address": "пл. Грецька, 3/4, ТЦ Афіна"
      },
      {
        "address": "пр. Добровольського, 137, ТРЦ \"Мой Дом\" "
      },
      {
        "address": "пр. Небесної Сотні (Маршала Жукова), 2, ТЦ Сіті Центр"
      },
      {
        "address": "Пантелеймонівська, 74"
      },
      {
        "address": "пл. Старосінна, 2"
      },
      {
        "address": "с. Фонтанка, ТРЦ Рив'єра"
      }
    ]
  },
  {
    "town": "Одеcа",
    "servicesCenters": [
      {
        "address": "вул. Генерала Бочарова, 40б"
      },
      {
        "address": "вул. Космонавтів, 32 "
      },
      {
        "address": "вул. Осипова, 25"
      },
      {
        "address": "вул. Пантелеймонівська, 25 ТРК Привоз "
      }
    ]
  },
  {
    "town": "Олевськ",
    "servicesCenters": [
      {
        "address": "вул. Привокзальна, 8 "
      }
    ]
  },
  {
    "town": "Олександрія",
    "servicesCenters": [
      {
        "address": "пр. Соборний (Леніна), 85"
      }
    ]
  },
  {
    "town": "Орджонікідзе",
    "servicesCenters": [
      {
        "address": "вул. Центральна (був. Калініна), 46 "
      }
    ]
  },
  {
    "town": "Острог",
    "servicesCenters": [
      {
        "address": "пр. Незалежності, 77а"
      }
    ]
  },
  {
    "town": "Очаків",
    "servicesCenters": [
      {
        "address": "вул. Лоцманська (був. Леніна), 28/1"
      }
    ]
  },
  {
    "town": "Первомайськ",
    "servicesCenters": [
      {
        "address": " вул. Грушевського, 19"
      }
    ]
  },
  {
    "town": "Первомайський",
    "servicesCenters": [
      {
        "address": "Мікро р-н 1/2, буд. 11"
      }
    ]
  },
  {
    "town": "Переяслав-Хмельницкий",
    "servicesCenters": [
      {
        "address": "вул. Богдана Хмельницького, б/н "
      }
    ]
  },
  {
    "town": "Підволочиськ",
    "servicesCenters": [
      {
        "address": "вул. Д. Галицького, 64 "
      }
    ]
  },
  {
    "town": "Покровськ",
    "servicesCenters": [
      {
        "address": "вул. Тургенєва, 6"
      }
    ]
  },
  {
    "town": "Полтава",
    "servicesCenters": [
      {
        "address": "вул. Зіньківська, 6/1а ТРЦ Київ"
      },
      {
        "address": "вул. Ковпака, 26 ТРЦ Екватор "
      },
      {
        "address": "вул. Соборності (Жовтнева), 53/1 "
      }
    ]
  },
  {
    "town": "Прилуки",
    "servicesCenters": [
      {
        "address": "вул. Київська, 172/1"
      }
    ]
  },
  {
    "town": "Радехів",
    "servicesCenters": [
      {
        "address": "вул. Львівська, 1а/1"
      }
    ]
  },
  {
    "town": "Радомишль",
    "servicesCenters": [
      {
        "address": "вул. Велика Житомирська, 4а"
      }
    ]
  },
  {
    "town": "Ратнe",
    "servicesCenters": [
      {
        "address": "вул. Центральна, 39 "
      }
    ]
  },
  {
    "town": "Рені",
    "servicesCenters": [
      {
        "address": "вул. Вознесенська (був. Комсомольська), 137"
      }
    ]
  },
  {
    "town": "Рівне",
    "servicesCenters": [
      {
        "address": "вул. Гагаріна, 16, ТРЦ \"Чайка\""
      },
      {
        "address": "вул. Міцкевича, 34"
      },
      {
        "address": " вул. Макарова, 23 ТРЦ \"Екватор\" "
      },
      {
        "address": "вул. Соборна, 190 В"
      },
      {
        "address": " вул. Соборна, 61"
      }
    ]
  },
  {
    "town": "Рогатин",
    "servicesCenters": [
      {
        "address": "вул. Галицька, 52-б "
      }
    ]
  },
  {
    "town": "Рожнятів",
    "servicesCenters": [
      {
        "address": "вул. Єдності, 3а "
      }
    ]
  },
  {
    "town": "Рокитне",
    "servicesCenters": [
      {
        "address": "вул. Кірова, 2б"
      }
    ]
  },
  {
    "town": "Ромни",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 1"
      }
    ]
  },
  {
    "town": "Самбір",
    "servicesCenters": [
      {
        "address": "пл. Ринок, 36 "
      }
    ]
  },
  {
    "town": "Сарни",
    "servicesCenters": [
      {
        "address": " вул. Широка, 37"
      }
    ]
  },
  {
    "town": "Світловодськ",
    "servicesCenters": [
      {
        "address": "вул. Героїв України (був. Леніна), 106 "
      }
    ]
  },
  {
    "town": "Сєвєродонецьк",
    "servicesCenters": [
      {
        "address": "вул. Хіміків, 32 "
      }
    ]
  },
  {
    "town": "Синельникове",
    "servicesCenters": [
      {
        "address": "вул. Виконкомівська (торговельний павільйон) "
      }
    ]
  },
  {
    "town": "Сквира",
    "servicesCenters": [
      {
        "address": " вул. Червона площа, 3"
      }
    ]
  },
  {
    "town": "Сколе",
    "servicesCenters": [
      {
        "address": "вул. Д. Галицького, 1"
      }
    ]
  },
  {
    "town": "Слов'янськ",
    "servicesCenters": [
      {
        "address": " вул. Університетська (був. Леніна), 37"
      }
    ]
  },
  {
    "town": "Сміла",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 89а "
      }
    ]
  },
  {
    "town": "Сокаль",
    "servicesCenters": [
      {
        "address": "вул. Шептицького, 50"
      }
    ]
  },
  {
    "town": "Сокиряни",
    "servicesCenters": [
      {
        "address": "вул. 28 червня, 7"
      }
    ]
  },
  {
    "town": "Старокостянтинів",
    "servicesCenters": [
      {
        "address": "вул. Острозького, 4"
      }
    ]
  },
  {
    "town": "Сторожинец",
    "servicesCenters": [
      {
        "address": "вул. О. Кобилянської, 1"
      }
    ]
  },
  {
    "town": "Стрий",
    "servicesCenters": [
      {
        "address": "м-н Ринок, 5 "
      }
    ]
  },
  {
    "town": "Суми",
    "servicesCenters": [
      {
        "address": " Набережна р. Стрілки, 10 "
      },
      {
        "address": " вул. Петропавлівська, 49"
      }
    ]
  },
  {
    "town": "Тальне",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 35"
      }
    ]
  },
  {
    "town": "Тараща",
    "servicesCenters": [
      {
        "address": "вул. Б. Хмельницького, 46"
      }
    ]
  },
  {
    "town": "Теребовля",
    "servicesCenters": [
      {
        "address": "вул. Князя Василька, 95"
      }
    ]
  },
  {
    "town": "Тересва",
    "servicesCenters": [
      {
        "address": "вул. Народна, 126а"
      }
    ]
  },
  {
    "town": "Терново",
    "servicesCenters": [
      {
        "address": " вул. Леніна, 96 "
      }
    ]
  },
  {
    "town": "Тернопiль",
    "servicesCenters": [
      {
        "address": "вул. Гетьмана Сагайдачного, 3"
      },
      {
        "address": "вул. Руська, 23 "
      },
      {
        "address": "вул. Руська,  33"
      },
      {
        "address": "вул. Текстильна, 28 ч, ТРЦ \"Подоляни\" "
      },
      {
        "address": " вул. Шептицького, 6"
      }
    ]
  },
  {
    "town": "Тетіїв",
    "servicesCenters": [
      {
        "address": "вул. Цвєткова, 2"
      }
    ]
  },
  {
    "town": "Токмак",
    "servicesCenters": [
      {
        "address": "вул. Революційна, 32 "
      }
    ]
  },
  {
    "town": "Томаківка",
    "servicesCenters": [
      {
        "address": "вул. Шосейна, 6 "
      }
    ]
  },
  {
    "town": "Тульчин",
    "servicesCenters": [
      {
        "address": "вул. Леонтовича (був. Леніна), 39"
      }
    ]
  },
  {
    "town": "Тячів",
    "servicesCenters": [
      {
        "address": "вул. Незалежності, 18"
      }
    ]
  },
  {
    "town": "Ужгород",
    "servicesCenters": [
      {
        "address": "пл. Кирила і Мефодія, 1 "
      },
      {
        "address": "вул. Корятовича, 12"
      }
    ]
  },
  {
    "town": "Узин",
    "servicesCenters": [
      {
        "address": " вул. Соборна (був.Першотравнева), 6-А, пав. 12"
      }
    ]
  },
  {
    "town": "Ульянівка",
    "servicesCenters": [
      {
        "address": "вул. Героїв України (був. Леніна), 58 "
      }
    ]
  },
  {
    "town": "Умань",
    "servicesCenters": [
      {
        "address": "вул. Європейська (був. Леніна), 7/1 "
      }
    ]
  },
  {
    "town": "Фастів",
    "servicesCenters": [
      {
        "address": "вул. Соборна, 28"
      }
    ]
  },
  {
    "town": "Харків",
    "servicesCenters": [
      {
        "address": "вул. 23 Серпня, 30"
      },
      {
        "address": "вул. Академіка Павлова, 321"
      },
      {
        "address": " вул. Валентинівська (Блюхера) "
      },
      {
        "address": "вул. Валентинівська (був. Блюхера), 21"
      },
      {
        "address": "пр. Гагаріна, 244-А"
      },
      {
        "address": "пр. Гагаріна, 22"
      },
      {
        "address": "вул. Героїв Праці, 7 ТРЦ Караван"
      },
      {
        "address": "вул. Героїв Праці, 9, ТРЦ Дафі"
      },
      {
        "address": " пр. Героїв Сталінграда, 134-3"
      },
      {
        "address": "майдан Захисників України, 1"
      },
      {
        "address": "вул. Конарєва (був. Червоноармійська), 8/10"
      },
      {
        "address": "пл. Конституції, 12"
      },
      {
        "address": "пр. Московський, 256, метро «Мосельського»"
      },
      {
        "address": "пр. Науки (був. Леніна), 9"
      },
      {
        "address": "пр.Олександрійський (був. Косіора)"
      },
      {
        "address": "пр. Петра Григоренка (був. Маршала Жукова), 7 "
      },
      {
        "address": " вул. Полтавський шлях, 148/2"
      },
      {
        "address": "вул. Різдвяна, 33"
      },
      {
        "address": "вул. Сумська, 72"
      },
      {
        "address": "пр. Ювілейний (був.50-річчя ВЛКСМ), 57а"
      }
    ]
  },
  {
    "town": "Херсон",
    "servicesCenters": [
      {
        "address": "вул. Іллюши Кулика, 122"
      },
      {
        "address": "вул. Залаегерсег, 18 "
      },
      {
        "address": "вул. Потьомкінська, 40/10 "
      }
    ]
  },
  {
    "town": "Хмельницький",
    "servicesCenters": [
      {
        "address": "вул. Героїв Майдану (був. Театральна)"
      },
      {
        "address": "вул. Кам'янецька, 17"
      },
      {
        "address": "вул. Проскурівська, 34"
      }
    ]
  },
  {
    "town": "Хмільник",
    "servicesCenters": [
      {
        "address": "пр. Свободи (був. вул. Леніна), 22/1"
      }
    ]
  },
  {
    "town": "Хуст",
    "servicesCenters": [
      {
        "address": "вул. Духновича, 2 "
      },
      {
        "address": "вул. Карпатської Січі, 32а"
      }
    ]
  },
  {
    "town": "Царичанка",
    "servicesCenters": [
      {
        "address": "вул. Театральна, 16"
      }
    ]
  },
  {
    "town": "Червоноград",
    "servicesCenters": [
      {
        "address": "вул. Шептицького, 12"
      }
    ]
  },
  {
    "town": "Черкаси",
    "servicesCenters": [
      {
        "address": "вул. Смілянська, 36 "
      },
      {
        "address": "вул.Шевченка, 385, ТРЦ \"Депот\""
      }
    ]
  },
  {
    "town": "Чернівці",
    "servicesCenters": [
      {
        "address": "пр. Незалежності, 113 "
      },
      {
        "address": "пл. Соборна, 9"
      }
    ]
  },
  {
    "town": "Чернігів",
    "servicesCenters": [
      {
        "address": "вул. 77 Гвардейської Дивізії, 1в "
      },
      {
        "address": "пр. Перемоги, 95, а-4"
      },
      {
        "address": "вул. Рокосовського, 25"
      }
    ]
  },
  {
    "town": "Чорнобай",
    "servicesCenters": [
      {
        "address": "вул. Леніна, 83"
      }
    ]
  },
  {
    "town": "Чорноморськ",
    "servicesCenters": [
      {
        "address": "пр. Миру, 18/23"
      },
      {
        "address": "пр.Миру"
      }
    ]
  },
  {
    "town": "Чортків",
    "servicesCenters": [
      {
        "address": "вул. Шевченка, 3"
      }
    ]
  },
  {
    "town": "Чугуїв",
    "servicesCenters": [
      {
        "address": "вул. Жадановського, 3а"
      }
    ]
  },
  {
    "town": "Чудей",
    "servicesCenters": [
      {
        "address": "вул. Карпатська, 1, ТД \"Прибутковий\""
      }
    ]
  },
  {
    "town": "Шепетівка",
    "servicesCenters": [
      {
        "address": "вул. Героїв Небесної Сотні (був. Карла Маркса), 35"
      }
    ]
  },
  {
    "town": "Шостка",
    "servicesCenters": [
      {
        "address": "вул. Свободи (К.Маркса), 22"
      }
    ]
  },
  {
    "town": "Шпола",
    "servicesCenters": [
      {
        "address": "вул. Соборна (Ленiна), 7"
      }
    ]
  },
  {
    "town": "Южноукраїнськ",
    "servicesCenters": [
      {
        "address": "просп. Незалежності, (був. Леніна), 22"
      }
    ]
  },
  {
    "town": "Ямпіль",
    "servicesCenters": [
      {
        "address": "вул. Свободи (був. Леніна), 140"
      }
    ]
  },
  {
    "town": "Ярмолинці",
    "servicesCenters": [
      {
        "address": "вул. Хмельницька, 6"
      }
    ]
  },
  {
    "town": "Ясиня",
    "servicesCenters": [
      {
        "address": "вул. Миру, 8"
      }
    ]
  }
]
}
